#Trabajo Práctico Integrador: Python 
#Realizar un script en Python para simular un juego de dados, según las siguientes condiciones: 
# El juego consistirá en lanzar dos dados. 

# Para simularlo, debemos obtener dos números aleatorios entre 1 y 6. 
# Si la suma de los dos dados es 4, el participante gana la mano 
# # Si la suma de los dos dados es menor a 4, el participante pierde y se termina el juego. 
#Realizar los comentarios pertinentes en el código para que se entienda la lógica utilizada. 
#Mostrar la suma de los números por pantalla y pedirle al usuario que toque una tecla para volver a lanzar los dados.


import random #importamos el modulo random que elije numeros al azar.

def tirar_dado(): #creados dos variables las cuales seran los dados, ahi saldran los numeros aleatorios
    num1 = random.choice([1,2,3,4,5,6])
    num2 = random.choice([1,2,3,4,5,6])
    if num1 + num2 == 4:
        print ("Los números que salieron fueron:", num1, "y", num2)
        print ("La suma de los mismos es:", num1+num2)
        print("Ganaste!")
    elif num1 + num2 < 4:
        print ("Los números que salieron fueron:", num1, "y", num2)
        print ("La suma de los mismos es:", num1+num2)
        print("Perdiste.")
    else:
        print("Los números que salieron fueron:", num1, "y", num2)
        print ("La suma de los mismos es:", num1+num2)

def confirmacion():
    cont = True  #creamos un bucle while para que la funcion se repira y no pare por si sola
    while cont:
        print("¿Desea tirar los datos nuevamente? Introduzca si o no")
        respuesta = input() #se introduce un dato
        if respuesta in ('si', 'sí', 'Si', 'Sí', 'SI', 'SÍ'): 
            tirar_dado() #coloco nuevamente la primer funcion para que lanze los dados nuevamente.
            cont = True #luego vuelve a comenzar el bucle para saber si el jugador quiere tirar dados nuevamente.
        elif respuesta in ('no', 'No', 'NO'): #si no quiere volver a jugar aqui se termine el juego, por ello colocamos el False.
            print("Gracias por participar, juego terminado")
            cont = False
        else: #en caso de colocar alguna respuesta que no este contemplada en nuestras opciones, volvemos a correr el bucle. Para ello colocamos el True, asi inicia nuevamente.
            print ("Respuesta incorrecta.")
            cont = True
    return cont

tirar_dado()  #ejecutamos las dos funciones.
confirmacion()
